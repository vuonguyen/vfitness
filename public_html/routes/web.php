<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('login', "Auth\AuthenticationController@login");
Route::post('login/authenticate', "Auth\AuthenticationController@authenticate");
Route::get('register', "Auth\AuthenticationController@register");
Route::post('register/store', "Auth\AuthenticationController@store");
Route::get('logout', 'Auth\AuthenticationController@logout');

Route::group(['middleware'=>'auth.sentinel'], function(){
	Route::get('/', [
		'as' => 'home',
		'uses' => 'UserController@home'
	]);

	Route::get('profile/{username}', 'UserController@profile');

	Route::get('{any}', 'PageController@view');
});

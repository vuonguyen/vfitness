<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//Route::group(['middleware'=>'auth.sentinel'], function(){
	Route::get('home', [
		'uses' => 'Api\ApiUserController@home'
	]);

	Route::get('profile/{username}', 'Api\ApiUserController@profile');
	Route::get('like/{user_id}/{status_id}', 'Api\ApiUserController@likeStatus');
	Route::post('status', 'Api\ApiStatusController@store');
	Route::get('friends/{username}', 'Api\ApiUserController@friends');
    Route::post('status/delete', 'Api\ApiStatusController@delete');

	//Route::get('{any}', 'PageController@view');
// });

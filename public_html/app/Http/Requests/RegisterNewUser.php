<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterNewUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:50',
			'last_name' => 'required|max:50',
			'username'  => 'required|alpha_dash|unique:users,username',
			'email' => 'required|email|unique:users,email',
			'password' => 'required|min:3|max:30',
			'password_confirmation' => 'required|same:password|min:3'
        ];
    }

	public function messages()
	{
		return [
			'first_name.required' => 'First name is required',
			'last_name.required' => 'Last name is required',
			'username.required'  => 'Username is required',
			'username.unique' => 'Username has been taken',
			'username.alpha_dash' => 'Username field only allows numbers, alphabet, underscore and dash',
 			'email.required' => 'Email is required',
			'email.uniqute' => 'This email has been registered before, you want to reset your password?',
			'password.required' => 'Password is required',
			'password_confirmation.same' => 'The confirmation password does not match',
			'password_confirmation.required' => 'The confirmation password is required',
		];
	}
}

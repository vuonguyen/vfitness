<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
//use Illuminate\Http\Resources\Json\ResourceCollection;

class StatusResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
			'id' => $this->id,
			'user' => $this->user,
			'content' => $this->content,
			'date' => $this->created_at->format('F d Y'),
			'likes' => $this->likes,
			'like_count' => $this->like_count,
			'published' => $this->published,
		];
    }
}

<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Status;
use App\Http\Resources\StatusResource;

class ApiStatusController extends Controller
{
    public function store(Request $request)
	{
		$status = $this->validate($request,[
			'userID' => 'required',
			'content' => 'required|max:160|',
		]);

		$input = [
			'user' => $request->userID,
			'content' => $request->content,
			'published' => 1
		];

		$newStatus = Status::create($input);

		return new StatusResource($newStatus);
	}

    public function delete(Request $request)
    {
        $updated = Status::where('id', $request->statusID)
                ->update(['published'=> 0]);

        $results = [];

        if($updated) $results['deleted'] = true;
        else $results['deleted'] = false;

        return $results;

    }
}

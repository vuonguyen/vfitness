<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Like;
use App\Models\Status;
use App\Http\Resources\StatusResource;
use App\Http\Resources\UserResource;

use Sentinel;

class ApiUserController extends Controller
{
	public function profile($username) {
		$user = User::where('username', $username)->first();
		$formattedStatuses = [];
		if( $user ) {
			$statuses = Status::where('user', $user->id)->orderBy('created_at', 'desc')->get();
			if ($statuses) {
				foreach( $statuses as $status ) {
					$likes = $status->likes;
					$status->like_count = count($likes);
					$formattedStatuses[] = new StatusResource($status);
				}
			}

			$formattedUser = new UserResource($user);
			$formattedUser['status'] = $formattedStatuses;

			return $formattedUser;
		}else {
			return null;
		}

	}
	/**
	 * Function that toggle Like/Unlike of a status from current user
	 * @param  [type] $userID   [the current user ID who is visiting this page]
	 * @param  [type] $statusID [the status that are liked or unliked]
	 * @return [type]           [new array of Likes for this status]
	 */
	public function likeStatus($userID, $statusID) {
		$like = new Like();
		//check whether this user has already liked this status
		$is_already_liked = $like->where('user_id', $userID)->where('status_id', $statusID)->exists();

		if( $is_already_liked ) { //if user already liked this status - remove it
			$like->where('user_id', $userID)->where('status_id', $statusID)->delete();
		}else { //if hasnt liked the status yet - like it
			$like->insert(
				['user_id' => $userID, 'status_id' => $statusID]
			);
		}

		//Then get the new like from the status
		$newLikes = Status::where('id', $statusID)->first()->likes()->get();

		return [
			'likes' => $newLikes,
			'likeCount' => count($newLikes) //update new number of likes for this post
		];
	}

	public function friends($username) {
		$user = User::where('username', $username)->first();
		$formattedFriends = [];
		if( $user ) {
			$friends = $user->friends;
			// foreach( $friends as $friend ) {
			// 	$formattedFriends[] = new UserResource($friend);
			// }
			return $friends;
		}else {
			return null;
		}
	}
}

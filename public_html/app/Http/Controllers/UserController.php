<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use Sentinel;

class UserController extends Controller
{

	//public $currentUser = null;

	public function __construct() {}

	/**
	 * Return home view when login
	 * @return view
	 */
    public function home(){
		$currentUser = Sentinel::check();
		return view('home', [
			'username' => $currentUser->username,
			'currentID' => $currentUser->id,
		]);
	}

	/**
	 * show profile of current user
	 * @return view
	 */
	public function profile($username) {
		$currentUser = Sentinel::check();
		return view('users.profile', [
			'username' => $username,
			'currentID' => $currentUser->id,
		]);
	}
}

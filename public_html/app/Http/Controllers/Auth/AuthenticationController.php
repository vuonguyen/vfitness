<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Http\Requests\RegisterNewUser;

use Illuminate\Http\Request;
use Sentinel;
use Activation;
use Validator;
use Session;

class AuthenticationController extends Controller
{
	public function __construct(){}

	public function login() {
		if(Sentinel::check()) {
			return redirect('home');
		}

		return view('users.login');
	}

	public function authenticate(Request $request) {
		$validatedDetails = $request->validate([
			'email' => 'required|email',
			'password' => 'required',
		]);

		$user = Sentinel::authenticate($validatedDetails);

		if($user) {
			return redirect('/');
		}else {
			return redirect('login')
					->withErrors(['Wrong credentials'])
					->withInput();
		}

	}

	public function logout() {
		Sentinel::logout();
		Session::flash('message', 'You have been logged out');
		return redirect('login');
	}

	public function register() {
		if( Sentinel::check() ) {
			return redirect('home');
		}

		return view('users.register');
	}

	public function store(RegisterNewUser $request) {
		$validatedDetails = $request->validated();
		$validatedDetails['avatar'] = 'http://i.pravatar.cc/150?img='.rand(1, 70);

		$user = Sentinel::registerAndActivate($validatedDetails);


		if( $user ) {
			Session::flash('message', 'You have registered successfully. Please log in');
			return redirect('login');
		}
	}

}

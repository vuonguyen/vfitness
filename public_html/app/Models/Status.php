<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'status';

	protected $fillable = [
		'user', 'content', 'published'
	];

	public function scopeIgnoreUnpublished($query) {
    	return $query->where('published', 1);
	}

	public function likes() {
		return $this->hasMany('App\Models\Like', 'status_id');
	}

	// public function likers() {
	// 	return $this->belongsToMany('App\Models\User', 'likes', 'user_id', 'status_id');
	// }
}

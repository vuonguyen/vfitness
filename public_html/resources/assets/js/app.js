
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';

window.Vue.use(VueRouter);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('profile-view', require('./components/ProfileComponent.vue'));
Vue.component('profile-friends', require('./components/FriendsComponent.vue'));
Vue.component('profile-status', require('./components/StatusComponent.vue'));

import HomeComponent from './components/HomeComponent.vue';
import ProfileComponent from './components/ProfileComponent.vue';

const routes = [
	{
		path: '/',
		components: {
			homeComponent: HomeComponent
		}
	},
	{
		path: '/profile/:username',
		components: {
			profileComponent: ProfileComponent
		}
	}
];

const router = new VueRouter({ routes });

const app = new Vue({ router }).$mount('#app');

// const app = new Vue({
//     el: '#app'
// });

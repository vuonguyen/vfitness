<!DOCTYPE html>
<html>
    <head>
    	<title>VFitness</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		{{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous"> --}}

        {{-- <link rel="stylesheet" href="/assets/css/app.css" /> --}}

        {{-- <link href="/assets/css/fileinput.css" media="all" rel="stylesheet" type="text/css" /> --}}

		<link rel="stylesheet" href="{{asset('css/app.css')}}">


    </head>
    <body>
    	@include("layout.header")
        <section class="container" id="app">
    		@yield("content")
        </section>
		@include("layout.footer")

		<script  src="{{asset('js/app.js')}}" charset="utf-8"></script>
		<script async src="https://use.fontawesome.com/19c0c206bd.js"></script>

		@yield('scripts')

    </body>

</html>

<header class="bg-light container-fluid">
	<div class="row align-items-center">
		<div class="col-6 text-center">
			<a class="navbar-brand" href="/">
				<img src="/images/logo.png" alt="VFitness logo" width="50">
			</a>
		</div>
		<div class="col-6">
			<nav class="navbar navbar-expand-lg navbar-light">
				<div class=" navbar-collapse">
					<ul class="navbar-nav mr-auto">
						@if ( $user = Sentinel::check() )
							<li class="nav-item">
								<a class="nav-link" href="/profile/{{$user['username']}}">
									<img class="rounded-circle" src="{{$user['avatar']}}" alt="" width="30">
									{{$user['first_name']}}
								</a>
							</li>
							<li class="nav-item">
	        					<a class="nav-link" href="/">Home</a>
	      					</li>
							<li class="nav-item">
	        					<a class="nav-link" href="/logout">Logout</a>
	      					</li>
						@else
							<li class="nav-item">
								<a class="nav-link" href="/login">Login</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="/register">Register</a>
							</li>
						@endif
      				</ul>
				</div>
			</nav>
		</div>
	</div>
</header>

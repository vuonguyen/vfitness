@extends('layout.master')

@section("content")
	@include('messages.errors')
	@include('messages.message')
	<div class="row justify-content-center">
		<div class="col-12 col-lg-10">
			<div class="card">
				<header class="card-header">
					<h6 class="card-title">Login</h6>
				</header>
				<div class="card-body">
					<form method="post" action="login/authenticate">
						{{ csrf_field() }}
						<div class="form-group">
							<label for="email">Email *</label>
							<input type="email" name="email" value="{{old('email')}}"class="form-control" id="email" placeholder="youremail@example.com" required>
						</div>
						<div class="form-group">
							<label for="password">Password *</label>
							<input type="password" name="password" class="form-control" id="password" placeholder="yourpassword" required>
						</div>
						<div class="form-group">
							<a href="/register">Register</a>
							<button class="btn btn-info pull-center">Log-in</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection

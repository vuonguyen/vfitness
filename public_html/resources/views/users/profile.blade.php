@extends('layout.master')

@section('content')
	<br>
	<div class="container">
		<profile-view
			user-name="{{$username}}"
			current-id = "{{$currentID}}"
			endpoint = "/api/profile/"
		></profile-view>

	</div>
@endsection

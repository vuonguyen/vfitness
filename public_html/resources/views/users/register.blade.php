@extends('layout.master')

@section ('content')
	@include('messages.errors')
	<section class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-lg-10">
				<div class="card">
					<header class="card-header">
						<h6 class="card-title">Register</h6>
					</header>
					<div class="card-body">
						<form method="post" action="register/store">
							{{ csrf_field() }}
							<div class="form-group">
								<input type="text" name="first_name" value="{{old('first_name')}}" class="form-control" placeholder="First name">
							</div>
							<div class="form-group">
								<input type="text" name="last_name" value="{{old('last_name')}}" class="form-control" placeholder="Last name">
							</div>
							<div class="form-group">
								<input type="email" name="email" value="{{old('email')}}" class="form-control" placeholder="email@example.com">
							</div>
							<div class="form-group">
								<input type="text" name="username" value="{{old('username')}}" class="form-control" placeholder="Username">
							</div>
							<div class="form-group">
								<input type="password" name="password" value="" class="form-control" placeholder="Password">
							</div>
							<div class="form-group">
								<input type="password" name="password_confirmation" value="" class="form-control" placeholder="Confirm password">
							</div>
							<div class="form-group">
								<button class="btn btn-info pull-center">Register</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

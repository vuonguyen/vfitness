@extends('layout.master')

@section('content')
	<router-view
		name="homeComponent"
		current-id = "{{$currentID}}"
		user-name="{{$username}}"
	></router-view>
    <router-view></router-view>
@endsection

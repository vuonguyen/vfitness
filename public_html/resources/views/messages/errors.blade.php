@if ($errors->any())
	<section class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-lg-10">
				<div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			</div>
		</div>
	</section>
@endif

@if(Session::has("message"))
	<section class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-lg-10">
    			<div class="alert alert-success">
        			<span>{{ Session::get('message') }}</span>
    			</div>
			</div>
		</div>
	</section>			
@endif

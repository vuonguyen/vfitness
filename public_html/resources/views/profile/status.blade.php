<article class="card">
	<div class="card-body">
		<h6 class="card-subtitle mb-2 text-muted">{{$status->updated_at->format('F d, Y')}}</h6>
		<p class="card-text">{{$status->content}}</p>
		<a class="btn btn-light" id="js-btn-like"><i class="fa fa-thumbs-up"></i></a>
		<a class="btn btn-light" id="js-btn-comment"><i class="fa fa-comments" aria-hidden="true"></i></a>
	</div>
</article>

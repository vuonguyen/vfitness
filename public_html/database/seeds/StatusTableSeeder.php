<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [1, 2];
		$status = new App\Models\Status();
		foreach( $users as $user ) {

			$newStatus = [
				'user' => $user,
				'content' => 'Curabitur a felis in nunc fringilla tristique. Phasellus nec sem in justo pellentesque facilisis. Aenean viverra rhoncus pede. Phasellus tempus. Cras sagittis.',
				'published' => 1,
			];

			$status->create($newStatus);
		}
	}
}

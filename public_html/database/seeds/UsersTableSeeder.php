<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$password = '@gency67';
		$users = [
			[
				'first_name' => 'Vee',
				'last_name'  => 'Kakalot',
				'email'      => 'vee@vnn.com',
				'avatar'     => 'http://i.pravatar.cc/200?img=3',
				'height'     => '175',
				'weight'     => '70',
				'username'   => 'vee.kakalot',
				'password'   => $password,
			],
			[
				'first_name' => 'Hollade',
				'last_name'  => 'Joshua',
				'email'      => 'aj@vnn.com',
				'avatar'     => 'http://i.pravatar.cc/150?img=15',
				'height'     => '195',
				'weight'     => '90',
				'username'   => 'bad-body-josh',
				'password'   => $password,
			],
		];
		$status = new App\Models\Status();
		foreach ($users as $user) {
			$registeredUser = Sentinel::registerAndActivate($user);
			$newStatus = [
				'user' => $registeredUser->id,
				'content' => 'Curabitur a felis in nunc fringilla tristique. Phasellus nec sem in justo pellentesque facilisis. Aenean viverra rhoncus pede. Phasellus tempus. Cras sagittis.',
				'published' => 1,
			];

			$status->create($newStatus);
		}
    }
}
